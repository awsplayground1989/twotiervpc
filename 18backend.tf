terraform {
  backend "s3" {
    bucket = "my-terraform-state-s3-witcher1"
    key    = "terraform-state/terraform.tfstate"
    region = "us-east-1"
  }
}
